var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var img64 = require('gulp-img64');
var del = require('del');
var runSequence = require('run-sequence');

var distPath = './dist/';

gulp.task('clean:dist', function (cb) {
    del([distPath + '**/*'], {force: true}, cb);
});

gulp.task('dist', function () {
    return gulp.src('./html/*.html')
        .pipe($.fileInclude({
            basepath: './'
        }))
        .pipe(img64())
        .pipe($.htmlmin({
            collapseWhitespace: true,
            removeComments: true,
            minifyJS: true,
            minifyCSS: true
        }))
        .pipe(gulp.dest(distPath));
    }
);

gulp.task('default', function (cb) {
    runSequence('dist', cb);
});